from django.contrib import admin
from django.urls import path, include
# from django.contrib.auth import (
#     login, logout
# )

from django.contrib.auth.views import LoginView, LogoutView

from django.conf.urls import url
from . import views
# from django.contrib.auth.views import (
#     login, logout, password_reset, password_reset_done, password_reset_confirm,
#     password_reset_complete
# )

app_name = "user"

urlpatterns = [
    path('register', views.register, name="register"),
    # path('login', LoginView.as_view(template_name='login.html'),name='login'),
    path('logout', LogoutView.as_view(), name='logout'),
    path('profile', views.view_profile, name="view_profile")
]
