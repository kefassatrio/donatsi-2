from django.conf.urls import url
from django.shortcuts import render, redirect
from django.urls import reverse

from .forms import (
    RegistrationForm,
    # EditProfileForm
)

from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from PermohonanDonasi.models import Permohonan


def register(request):
    if request.method =='POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            # print(reverse('permohonan:list-permohonan'))
            # return redirect(reverse('permohonan:list-permohonan'))
            return redirect(reverse('Login:login'))
    else:
        form = RegistrationForm()

    args = {'form': form}
    return render(request, 'reg_form.html', args)

@login_required(login_url='/login')
def view_profile(request):
    user = request.user
    recommendations = Permohonan.objects.filter(pemilik=request.user.username)
    args = {
        'user': user,
        'recommendations' : recommendations
    }
    return render(request, 'profile.html', args)

# def logout(request):
#     logout(request)

# def view_profile(request, pk=None):
#     if pk:
#         user = User.objects.get(pk=pk)
#     else:
#         user = request.user
#     args = {'user': user}
#     return render(request, 'accounts/profile.html', args)

# def edit_profile(request):
#     if request.method == 'POST':
#         form = EditProfileForm(request.POST, instance=request.user)

#         if form.is_valid():
#             form.save()
#             return redirect(reverse('accounts:view_profile'))
#     else:
#         form = EditProfileForm(instance=request.user)
#         args = {'form': form}
#         return render(request, 'accounts/edit_profile.html', args)
