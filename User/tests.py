from django.test import TestCase, Client
# from PermohonanDonasi.models import Permohonan
# from PemberianDonasi.models import Pemberian
from .models import UserProfile
from django.contrib.auth.models import User

# Create your tests here.
class PemberianDonasiTest(TestCase):
    # def test_fail(self):
    #     self.fail("fail oiiii")
    def setUp(self):
        self.c = Client()
        self.user = User.objects.create_user(username='john',
                                 email='jlennon@beatles.com',
                                 password='glassonion')
        self.userProfile = UserProfile(user=self.user)

    def test_object_to_string(self):
        self.assertEqual(str(self.userProfile), self.userProfile.user.username)

    

    def test_register_url(self):
        response = self.c.get('/user/register')
        self.assertEqual(response.status_code, 200)

    def test_redirect_logout(self):
        response = self.c.get('/user/logout')
        self.assertEqual(response.status_code, 302)

    def test_redirect_blm_login_profile(self):
        response = self.c.get('/user/logout')
        self.assertEqual(response.status_code, 302)


    # # def test_ada_tag_form(self):
    # #     response = self.c.get('/donasi/'+str(self.permohonan.id)+"/beri-donasi")
    # #     self.assertTemplateUsed(response, 'daftar-pemberian.html')
    
    # # def test_apakah_punya_form(self):
    # #     response = self.c.get('/donasi/'+str(self.permohonan.id)+"/beri-donasi")
    # #     content = response.content.decode('utf8')
    # #     self.assertIn('<form', content)
        
    def test_bikin_user(self):
        response = self.c.post('/user/register',{
           'username': "kefassatrio",
            'first_name': "kefas",
            'last_name': "satrio",
            'email':"kefas@gmail.com",
            'password1':"123bangkit45",
            'password2': "123bangkit45"
        })
        self.assertEqual(response.status_code, 302)

    def test_bikin_user_salah(self):
        response = self.c.post('/user/register',{
           'username': "kefassatrio",
            'first_name': "kefas",
            'last_name': "satrio",
            'email':"kefas@gmail.com",
            'password1':"123bangkit45",
            'password2': "kefassatrio"
        })
        self.assertEqual(response.status_code, 200)

    def test_masuk_fungsi_myprofile(self):
        self.client.login(username='john', password='glassonion')
        response = self.client.get('/user/profile')
        self.assertEqual(response.status_code, 200)
    # # self.assertTemplateUsed(response.get, 'semua-permohonan.html')