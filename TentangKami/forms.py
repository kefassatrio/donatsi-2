from django import forms
from . import models

class CreateSaran(forms.ModelForm):
    class Meta:
        model = models.Saran
        fields = ["saran"]
        exclude = ["pemilik"]
