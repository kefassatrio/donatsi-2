from django.shortcuts import render, redirect
from .forms import CreateSaran
from .models import Saran
from django.contrib.auth.decorators import login_required


# @login_required(login_url='/login')
def tentang_kami(request):
    obj = Saran.objects.all()
    if(request.method == "POST"):
        if(not request.user.is_authenticated):
            return redirect("Login:login")
            
        form = CreateSaran(request.POST)
        if form.is_valid():
            komen = form.save(commit=False)
            komen.pemilik = request.user.username
            komen.save()
    else:
        form = CreateSaran()
    return render(request, "tentang-kami.html", {"form": form, "comments" : obj})