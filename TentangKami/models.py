from django.db import models

# Create your models here.
class Saran(models.Model):
    saran = models.TextField(max_length=500)
    pemilik = models.CharField(max_length=20)
    
    def __str__(self):
        if(len(self.saran)>20):
            return self.saran[:20]
        else:
            return self.saran