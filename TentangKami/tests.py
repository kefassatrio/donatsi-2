from django.test import TestCase
from django.test import Client
from django.urls import resolve
from PermohonanDonasi.models import Permohonan
from .forms import CreateSaran
from .models import Saran
from .views import tentang_kami
from django.http import HttpRequest
from datetime import date
from User.models import User, UserProfile
import unittest


# Create your tests here

class TestTentangKami(TestCase):
    def setUp(self):
        self.c = Client()
        self.user = User.objects.create_user(username='john',
                                 email='jlennon@beatles.com',
                                 password='glassonion')
        self.userProfile = UserProfile(user=self.user)
    
    def test_masuk_bikin_komen(self):
        self.client.login(username='john', password='glassonion')
        response = self.client.post('/tentang-kami/',{
            "saran":"saran"})
        self.assertEqual(response.status_code, 200)


    def test_url_exist(self):
        c = Client()
        response = c.get('/tentang-kami/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_dipake(self):
        c = Client()
        response = c.get('/tentang-kami/')
        self.assertTemplateUsed(response, 'tentang-kami.html')
    
    def test_pake_view(self):
        handler = resolve('/tentang-kami/')
        self.assertEqual(handler.func, tentang_kami)

    def test_has_input(self):
        c = Client()
        response = c.get('/tentang-kami/')
        content = response.content.decode('utf8')
        self.assertIn('<input', content)

    def test_forms_comments(self):
        data_form = {'saran':'saran'}
        form = CreateSaran(data = data_form)
        self.assertTrue(form.is_valid())


    def test_model(self):
        saran = Saran.objects.create(saran="saran")
        self.assertEqual('saran', str(saran))

    def test_model_panjang(self):
        saran = Saran.objects.create(saran="sarannnnnnnnnnnnnnnnnnnnnnn")
        self.assertEqual('sarannnnnnnnnnnnnnnn', str(saran))


    def test_post_redirect_blm_login(self):
        c = Client()
        response = c.post('/tentang-kami/',{
            "saran":"saran"
        })
        self.assertEqual(response.status_code, 302)