from django.urls import path, include
from .views import tentang_kami

app_name = "tentang-kami"
urlpatterns = [
    path('', tentang_kami, name="about")
]
