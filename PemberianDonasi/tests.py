from django.test import TestCase, Client
from PermohonanDonasi.models import Permohonan
from PemberianDonasi.models import Pemberian
from User.models import User, UserProfile

# Create your tests here.
class PemberianDonasiTest(TestCase):
    # def test_fail(self):
    #     self.fail("fail oiiii")
    def setUp(self):
        self.c = Client()
        self.permohonan = Permohonan(nama_pemohon="Kefas Satrio", email="kefassatrio@gmail.com",
                                bank="BCA", rekening=12345678910, nama_donasi="beliin telur dong mau bikin donat",
                                target_uang=2500000, uang_terpenuhi=0, deadline="2020-12-12", deskripsi_alasan="mau makan"
                                )
        self.permohonan.save()
        self.pemberian = Pemberian(donasi=self.permohonan, nama_pemberi="Bukan Kefas", email="bukankefas@gmail.com", nomor_telp="0812312331", jumlah_uang=69420, kartu_kredit="123123213")
        
        # self.c = Client()
        self.user = User.objects.create_user(username='john',
                                 email='jlennon@beatles.com',
                                 password='glassonion')
        self.userProfile = UserProfile(user=self.user)
    
    # def test_get_pemberian_donasi(self):
    #     self.client.login(username='john', password='glassonion')
    #     response = self.client.get('/donasi/12/beri-donasi')
    #     self.assertEqual(response.status_code, 200)

    def test_get_dengan_login(self):
        self.client.login(username='john', password='glassonion')
        response = self.client.get('/donasi/'+ str(self.permohonan.id) +'/beri-donasi')
        self.assertEqual(response.status_code, 200)

    def test_post_dengan_login(self):
        self.client.login(username='john', password='glassonion')
        response = self.client.post('/donasi/'+ str(self.permohonan.id) +'/beri-donasi',{
            'donasi':self.permohonan, 
            'nama_pemberi':"Bukan Kefas", 
            'email':"bukankefas@gmail.com", 
            'nomor_telp':"0812312331", 
            'jumlah_uang':69420, 
            'kartu_kredit':"123123213"
        })
        self.assertEqual(response.status_code, 302)
        

    def test_object_to_string(self):
        self.assertEqual(str(self.pemberian), self.pemberian.nama_pemberi)

    def test_redirect_login(self):
        response = self.c.get('/donasi/12/beri-donasi')
        self.assertEqual(response.status_code, 302)

    # def test_ada_tag_form(self):
    #     response = self.c.get('/donasi/'+str(self.permohonan.id)+"/beri-donasi")
    #     self.assertTemplateUsed(response, 'daftar-pemberian.html')
    
    # def test_apakah_punya_form(self):
    #     response = self.c.get('/donasi/'+str(self.permohonan.id)+"/beri-donasi")
    #     content = response.content.decode('utf8')
    #     self.assertIn('<form', content)
        
    # def test_bikin_object(self):
    #     c = Client()
    #     response = c.post('/donasi/'+ str(self.permohonan.id) +'/beri-donasi',{
    #         'donasi':self.permohonan, 
    #         'nama_pemberi':"Bukan Kefas", 
    #         'email':"bukankefas@gmail.com", 
    #         'nomor_telp':"0812312331", 
    #         'jumlah_uang':69420, 
    #         'kartu_kredit':"123123213"
    #     })
    #     self.assertEqual(response.status_code, 302)

    # self.assertTemplateUsed(response.get, 'semua-permohonan.html')