from django.db import models

# Create your models here.
class Pemberian(models.Model):
    donasi = models.ForeignKey("PermohonanDonasi.Permohonan", on_delete=models.CASCADE)
    nama_pemberi = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    nomor_telp = models.CharField(max_length=13)
    jumlah_uang = models.PositiveIntegerField()
    kartu_kredit = models.CharField(max_length=50)

    def __str__(self):
        return self.nama_pemberi