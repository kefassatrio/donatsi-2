from django.shortcuts import render, redirect
from PermohonanDonasi.models import Permohonan
from .forms import CreatePemberian

from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required(login_url='/login')
def daftar_pemberian(request, id):
    if(request.method == "POST"):
        form = CreatePemberian(request.POST)
        if form.is_valid():
            pemberian = form.save(commit=False)
            donasi = Permohonan.objects.get(id=id)
            donasi.uang_terpenuhi += pemberian.jumlah_uang
            donasi.save()
            pemberian.donasi = donasi
            pemberian.save()
            return redirect("permohonan:detail-permohonan", id=donasi.id)
    
    form = CreatePemberian()
    return render(request, "daftar-pemberian.html", {"form": form})