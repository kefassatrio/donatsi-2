from django import forms
from . import models
from django.utils import timezone
from datetime import datetime


class DateInput(forms.DateInput):
    input_type = "date"


class CreatePermohonan(forms.ModelForm):
    class Meta:
        model = models.Permohonan
        fields = "__all__"
        exclude = ['uang_terpenuhi', 'kategori_alasan', 'urgency', 'comments', 'semua_pemberian', 'tanggal_pembuatan', 'pemilik']

        widgets = {
            "deadline": DateInput,
            "target_uang": forms.TextInput()
        }

    def __init__(self, *args, **kwargs):
        super(CreatePermohonan, self).__init__(*args, **kwargs)

        for fname, f in self.fields.items():
            f.widget.attrs['class'] = 'input'

        self.fields['pilihan_kategori_alasan'].widget.attrs['class'] += ' pilihan-kategori'
        self.fields['nama_donasi'].widget.attrs['class'] += ' nama-donasi'
        self.fields['deskripsi_alasan'].widget.attrs['class'] += ' deskripsi-alasan'
        self.fields['image_donasi'].widget.attrs['class'] += ' image-donasi'


        

# class SearchPermohonan(forms.Form):
#     searched = forms.CharField(max_length=100)