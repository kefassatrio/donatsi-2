from django.contrib import admin
from django.urls import path, include
from .views import daftar_permohonan, list_permohonan, detail_permohonan, getData

app_name = "permohonan"
urlpatterns = [
    path('', list_permohonan, name="list-permohonan"),
    path('getData/', getData, name="get-data"),
    path('<id>', detail_permohonan, name="detail-permohonan"),
    path('daftar/', daftar_permohonan, name="daftar-permohonan"),
]
