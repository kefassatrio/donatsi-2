from django.test import TestCase, Client
from .models import Permohonan
from .views import homepage, getData
from django.urls import reverse
import django.utils.timezone
from django.contrib.auth.models import User
from PemberianDonasi.models import Pemberian
from User.models import User, UserProfile

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class PermohonanDonasiTest(TestCase):
    def setUp(self):
        self.c = Client()


        self.user = User.objects.create_user(username='john',
                                 email='jlennon@beatles.com',
                                 password='glassonion')
        self.userProfile = UserProfile(user=self.user)
    # def test_fail(self):
    #     self.fail("fail oiiii")
    
    # def setUp(self):
    #     self.client = Client()
    #     options = Options()
    #     options.add_argument('--headless')
    #     options.add_argument('--no-sandbox')
    #     options.add_argument('--disable-dev-shm-usage')
    #     self.browser = webdriver.Chrome(chrome_options=options, executable_path="chrome_77_driver/chromedriver.exe")
    #     # self.browser = webdriver.Chrome(chrome_options=options)
    
    # def tearDown(self):
    #     self.browser.close()
        
    # FUNCTIONAL TEST

    # def testSearch(self):
    #     self.browser.get('http://localhost:8000/donasi/')
    #     searchInput = self.browser.find_element_by_id("search_input")
    #     searchButton = self.browser.find_element_by_id("search_submit")
    #     searchInput.send_keys("tes")
    #     searchButton.click()
    #     self.assertIn("tes", self.browser.page_source)
    #     self.assertIn("TEST123123", self.browser.page_source)

    # def testJsonResponse(self):
    #     self.browser.get('http://localhost:8000/donasi/getData')
    #     self.assertIn('{"model":', self.browser.page_source)


    # UNITTEST
    def test_getData(self):
        c = Client()
        response = c.get('/donasi/getData/')
        self.assertEqual(response.status_code, 200)
    
    def test_fungsi_getData(self):
        c = Client()
        response = getData(c)
        self.assertEqual(response._content_type_for_repr[3:-1], 'text/json-comment-filtered')
    
    #######################################################

    def test_open_donasi_tanpa_login(self):
        c = Client()
        response = c.get('/donasi/daftar/')
        self.assertEqual(response.status_code, 302)
    
    def test_open_donasi_dengan_login(self):
        self.client.login(username='john', password='glassonion')
        response = self.client.get('/donasi/daftar/')
        self.assertEqual(response.status_code, 200)
    
    def test_post_open_donasi_dengan_login(self):
        self.client.login(username='john', password='glassonion')
        response = self.client.post('/donasi/daftar/')
        self.assertEqual(response.status_code, 200)
    
    def test_post_open_donasi_dengan_valid(self):
        self.client.login(username='john', password='glassonion')
        response = self.client.post('/donasi/daftar/',{
            "nama_pemohon":"Kefas Satrio",
            "email": "kefassatrio@gmail.com",
            "bank": "BCA",
            "rekening": 12345678910,
            "nama_donasi": "beliin telur dong mau bikin donat",
            "target_uang": 2500000,
            "uang_terpenuhi": 0,
            "deadline": "2020-12-12",
            "deskripsi_alasan": "mau makan"})
        self.assertEqual(response.status_code, 200)

    # def test_get_detail_donasi(self):
    #     permohonan = Permohonan(nama_pemohon="Kefas Satrio", email="kefassatrio@gmail.com",
    #                     bank="BCA", rekening=12345678910, nama_donasi="beliin telur dong mau bikin donat",
    #                     target_uang=2500000, uang_terpenuhi=0, deadline="2020-12-12", deskripsi_alasan="mau makan"
    #                     )
    #     self.client.login(username='john', password='glassonion')
    #     response = self.client.get('/donasi/1')
    #     self.assertEqual(response.status_code, 200)


    def test_object_to_string(self):
        permohonan = Permohonan(nama_pemohon="Kefas Satrio", email="kefassatrio@gmail.com",
                                bank="BCA", rekening=12345678910, nama_donasi="beliin telur dong mau bikin donat",
                                target_uang=2500000, uang_terpenuhi=0, deadline="2020-12-12", deskripsi_alasan="mau makan"
                                )
        self.assertEqual(str(permohonan), permohonan.nama_donasi)

    def test_default_image(self):
        permohonan = Permohonan(nama_pemohon="Kefas Satrio", email="kefassatrio@gmail.com",
                                bank="BCA", rekening=12345678910, nama_donasi="beliin telur dong mau bikin donat",
                                target_uang=2500000, uang_terpenuhi=0, deadline="2020-12-12", deskripsi_alasan="mau makan"
                                )
        self.assertEqual(permohonan.image_donasi, "images/donatsi.png")

    def test_default_tanggal_pembuatan(self):
        permohonan = Permohonan(nama_pemohon="Kefas Satrio", email="kefassatrio@gmail.com",
                                bank="BCA", rekening=12345678910, nama_donasi="beliin telur dong mau bikin donat",
                                target_uang=2500000, uang_terpenuhi=0, deadline="2020-12-12", deskripsi_alasan="mau makan"
                                )
        self.assertEqual(permohonan.tanggal_pembuatan.strftime("%b %d %Y"), django.utils.timezone.now().strftime("%b %d %Y"))

    def test_default_pilihan_kategori_alasan(self):
        permohonan = Permohonan(nama_pemohon="Kefas Satrio", email="kefassatrio@gmail.com",
                                bank="BCA", rekening=12345678910, nama_donasi="beliin telur dong mau bikin donat",
                                target_uang=2500000, uang_terpenuhi=0, deadline="2020-12-12", deskripsi_alasan="mau makan"
                                )
        self.assertEqual(permohonan.pilihan_kategori_alasan, "kegiatan_sosial")

    def test_singkatan_deskripsi(self):
        permohonan = Permohonan(nama_pemohon="Kefas Satrio", email="kefassatrio@gmail.com",
                                bank="BCA", rekening=12345678910, nama_donasi="beliin telur dong mau bikin donat",
                                target_uang=2500000, uang_terpenuhi=0, deadline="2020-12-12", deskripsi_alasan="mauuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu maaaaaaaakannnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn"
                                )
        self.assertEqual(permohonan.deskripsi_singkat(), permohonan.deskripsi_alasan[:50]+"...")

    ########################################
    def test_apakah_ada_url_slash_donasi(self):
        c = Client()
        response = c.get('/donasi/')
        self.assertEqual(response.status_code, 200) #200 = OK

    def test_apakah_ada_url_slash_donasi_daftar(self):
        c = Client()
        response = c.get('/donasi/')
        self.assertEqual(response.status_code, 200) #200 = OK

    def test_apakah_langsung_redirect_login(self):
        c = Client()
        response = c.get('/donasi/daftar/')
        self.assertEqual(response.status_code, 302)

    
    def test_template_semua_permohonan(self):
        c = Client()
        response = c.get('/donasi/')
        self.assertTemplateUsed(response, 'semua-permohonan.html')
    
    def test_bikin_object(self):
        c = Client()
        response = c.post('/donasi/daftar/',{
            "nama_pemohon":"Kefas Satrio",
            "email": "kefassatrio@gmail.com",
            "bank": "BCA",
            "rekening": 12345678910,
            "nama_donasi": "beliin telur dong mau bikin donat",
            "target_uang": 2500000,
            "uang_terpenuhi": 0,
            "deadline": "2020-12-12",
            "deskripsi_alasan": "mau makan"
        })
        self.assertEqual(response.status_code, 302)

    def test_fungsi_deskripsi_singkat(self):
        c = Client()
        test = Permohonan(
            nama_pemohon = "Kefas Satrio",
            email =  "kefassatrio@gmail.com",
            bank =  "BCA",
            rekening =  "12345678910",
            nama_donasi =  "beliin telur dong mau bikin donat",
            target_uang =  2500000,
            uang_terpenuhi =  0,
            deadline =  "2020-12-12",
            deskripsi_alasan =  "mau makan"
        )
        desc = test.deskripsi_singkat()
        self.assertEqual(desc, "mau makan")

    
    def test_fungsi_homepage_urgency(self):
        c = Client()
        test = Permohonan(
            nama_pemohon = "Kefas Satrio",
            email =  "kefassatrio@gmail.com",
            bank =  "BCA",
            rekening =  "12345678910",
            nama_donasi =  "beliin telur dong mau bikin donat",
            target_uang =  2500000,
            uang_terpenuhi =  0,
            deadline =  "2020-12-12",
            deskripsi_alasan =  "mau makan",
            urgency = 5
        )
        test.save()
        homepage(c)
        response = c.get('/')
        content = response.content.decode('utf8')
        self.assertIn('beliin telur dong mau bikin donat', content)
    
    def test_fungsi_homepage_delete(self):
        c = Client()
        test = Permohonan(
            nama_pemohon = "Kefas Satrio",
            email =  "kefassatrio@gmail.com",
            bank =  "BCA",
            rekening =  "12345678910",
            nama_donasi =  "beliin telur dong mau bikin donat",
            target_uang =  2500000,
            uang_terpenuhi =  0,
            deadline =  "2000-12-12",
            deskripsi_alasan =  "mau makan",
            urgency = 5
        )
        test.save()
        homepage(c)
        ada = False
        for permohonan in Permohonan.objects.all():
            if permohonan.nama == "beliin telur dong mau bikin donat":
                ada = True
        self.assertFalse(ada)
    
