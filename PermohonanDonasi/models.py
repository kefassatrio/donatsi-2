from django.db import models
import django.utils.timezone
from KategoriDonasi.models import Kategori
# from User.models import UserProfile

# Create your models here.

class Permohonan(models.Model):
    KATEGORI = []
    for kategori in Kategori.objects.all():
        KATEGORI.append((kategori.key_kategori, kategori.nama_kategori))


    email = models.EmailField(max_length=100)
    nama_pemohon = models.CharField(max_length=100)
    bank = models.CharField(max_length=100)
    rekening = models.CharField(max_length=100)
    nomor_telp = models.CharField(null=True, max_length=13)
    nama_donasi = models.CharField(max_length=100)
    image_donasi = models.ImageField(upload_to="images/", default="images/donatsi.png")
    target_uang = models.PositiveIntegerField()
    uang_terpenuhi = models.PositiveIntegerField(default=0, null=True)
    deadline = models.DateField(auto_now_add = False)

    pemilik = models.CharField(max_length=100)

    pilihan_kategori_alasan = models.CharField(max_length=100, choices=KATEGORI,
        default='kegiatan_sosial'
    )

    kategori_alasan = models.ForeignKey('KategoriDonasi.Kategori', null=True, on_delete=models.CASCADE)
    urgency = models.PositiveIntegerField(default=0, null=True)
    deskripsi_alasan = models.TextField(max_length=500)
    # comments = models.ForeignKey('Comments.data_comments', on_delete=models.CASCADE, default=None)
    # semua_pemberian = models.ForeignKey('PemberianDonasi.Pemberian', on_delete=models.CASCADE)
    tanggal_pembuatan = models.DateField(default = django.utils.timezone.now)

    def __str__(self):
        return self.nama_donasi
    
    def deskripsi_singkat(self):
        if(len(self.deskripsi_alasan) > 50):
            return self.deskripsi_alasan[:50] + "..."
        return self.deskripsi_alasan