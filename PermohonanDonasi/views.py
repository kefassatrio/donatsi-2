from django.shortcuts import render, redirect
from django.core import serializers
from django.http import HttpResponse

from .forms import CreatePermohonan
from KategoriDonasi.models import Kategori
from .models import Permohonan
from Comments.forms import add_comment
from Comments.models import data_comments

from django.contrib.auth.decorators import login_required


import datetime

# Create your views here.
def getData(request):
    permohonan = Permohonan.objects.all()
    permohonan_list = serializers.serialize('json', permohonan)
    return HttpResponse(permohonan_list, content_type="text/json-comment-filtered")
    
def homepage(request):
    recommendations = []
    
    for permohonan in Permohonan.objects.all():
        if permohonan.deadline <= datetime.date.today():
            permohonan.delete()
    
    for i in range(5,0,-1):
        permohonan_query = Permohonan.objects.filter(urgency = i)
        for permohonan in permohonan_query:
            if(len(recommendations) < 6):
                recommendations.append(permohonan)
            else:
                break
        if(len(recommendations) >= 6):
            break

    return render(request,"homepage.html", {"recommendations": recommendations})

@login_required(login_url='/login')
def daftar_permohonan(request):
    form = CreatePermohonan()
    if request.method == "POST":
        form = CreatePermohonan(request.POST, request.FILES)
        if form.is_valid():
            permohonan = form.save(commit=False)
            permohonan.kategori_alasan = Kategori.objects.get(key_kategori=permohonan.pilihan_kategori_alasan)
            permohonan.urgency = permohonan.kategori_alasan.urgency
            permohonan.pemilik = request.user.username
            permohonan.save()
            return redirect("homepage")
    return render(request, "daftar-permohonan.html", {'form': form})

def list_permohonan(request):
    # query = request.GET.get("query")
    # if query:
    #     objects = Permohonan.objects.filter(nama_donasi__contains=query)
    # else:
    #     objects = Permohonan.objects.all()
    
    objects = Permohonan.objects.all()
    for permohonan in objects:
        if permohonan.deadline <= datetime.date.today():
            permohonan.delete()

    return render(request, 'semua-permohonan.html')

# @login_required(login_url='/login')
def detail_permohonan(request, id):
    permohonan = Permohonan.objects.get(id=id)
    if(request.method == "POST"):
        if(not request.user.is_authenticated):
            return redirect("Login:login")
        
        form = add_comment(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.permohonan = permohonan
            comment.pemilik = request.user.username
            comment.save()
            return redirect("permohonan:detail-permohonan", id=id)
    
    form = add_comment()
    comments = data_comments.objects.filter(permohonan=permohonan).order_by("dateTime").reverse()
    return render(request, 'detail-permohonan.html', {"permohonan": permohonan, "comments": comments, "form": form})

