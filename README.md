![pipeline status](https://gitlab.com/kefassatrio/donatsi-2/badges/master/pipeline.svg) ![coverage report](https://gitlab.com/kefassatrio/donatsi-2/badges/master/coverage.svg)

#TUGAS KELOMPOK 2 PPW

# Kelompok PPW KE03:
- Kefas Satrio Bangkit Solideantyo
- Hanna Jannatunna'iim
- Nur Fauziah Hasanah
- Anthony Martin

# Link Herokuapp:
## https://donatsi-2.herokuapp.com/

# Tentang Aplikasi Ini:
### Aplikasi ini adalah sebuah platform yang memperbolehkan seorang user untuk dapat meminta donasi atau memberikan donasi kepada user-user yang lain.

- Jika seorang user ingin meminta donasi maka user akan disuruh aplikasi untuk memilih tipe donasi, misalnya untuk perawatan kesehatan, untuk charity, untuk bencana, atau untuk yang lainnya.
Selain itu, aplikasi akan meminta kepada user beberapa hal seperti identitas, bank pilihan, rekening user, jumlah uang yang diperlukan, dan deskripsi tentang alasan mengapa memerlukan donasi tersebut yang meminta donasi tersebut.

- Untuk user yang ingin mendonasi, aplikasi akan menyediakan homepage yang berisi *recommendations* tentang permohonan donasi apa yang mungkin lebih penting dari yang lain
dengan cara mengurutkan *urgency* semua permohonan donasi yang ada dan aplikasi akan menampilkan
permonohan-permohonan donasi dari yang memiliki *urgency* tertinggi sampai terendah,
contohnya, permohonan donasi untuk daerah yang terkena bencana akan memiliki *urgency* lebih tinggi dari permohonan donasi untuk charity sehingga user akan lebih direkomendasikan untuk mendonasi kepada daerah yang terkena bencana.
User yang ingin mendonasi akan diminta untuk memberikan identitas, nomor kartu, dan jumlah uang yang ingin diberikan untuk donasi.

- Dalam homepage user dapat search nama donasi yang mungkin ada dalam database.

- Jika sebuah permohonan donasi yang ada di homepage ditekan user, maka user akan di-redirect ke suatu halaman yang berisi detail tentang donasi tersebut dan di dalam halaman tersebut,
user dapat membuat comment untuk mungkin bertanya sesuatu kepada pemohon donasi dan pemohon donasi dapat menjawabnya.
