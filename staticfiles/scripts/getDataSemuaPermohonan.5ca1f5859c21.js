const myUrl = "https://donatsi-2.herokuapp.com"

const formatPermohonan = (permohonan) => {
    deskripsi = permohonan.fields.deskripsi_alasan.length >50 ? permohonan.fields.deskripsi_alasan.substring(0,50) + "..." : permohonan.fields.deskripsi_alasan;
        $("#sp_result").append(
            `<div class="col-xl-3 col-md-4 col-12 mb-4" id="sp_card">
                <div class="card h-100">
                    <img class="card-img-top" src="/media/${ permohonan.fields.image_donasi }" alt="image">
                    <div class="card-body">
                        <h4 class="card-title">${ permohonan.fields.nama_donasi }</h4>
                        <p class="card-text">Kategori: ${ permohonan.fields.pilihan_kategori_alasan }</p>
                        <p class="card-text">Deskripsi: ${ deskripsi }</p>
                        <h5 class="card-text duit">Rp. ${ permohonan.fields.uang_terpenuhi } / Rp. ${ permohonan.fields.target_uang }</h5>
                        <p class="card-text">Deadline: ${ permohonan.fields.deadline }</p>
                        <a href="/donasi/${ permohonan.pk }/beri-donasi"><button class="submit">Donate</button></a>
                        <a href="/donasi/${ permohonan.pk }"><p id="details" class="permohonan-details">Details</p></a>            
                    </div>
                </div>
            </div>`
        );
}

const formatSemuaPermohonan = (data) => {  
    data.forEach(function(permohonan, index){
        formatPermohonan(permohonan)
    })
}
const formatSearchRecommendation = (data, key) => {
    data.forEach(function(permohonan, index){
        if(permohonan.fields.nama_donasi.toLowerCase().includes(key.toLowerCase()) || permohonan.fields.deskripsi_alasan.toLowerCase().includes(key.toLowerCase())){
            formatPermohonan(permohonan);
        }  
    })
}

$(document).ready(() => {
    // $('#results')[0].innerHTML = "<h3> Loading page... </h3>";
    $.ajax({
        method: 'GET',
        url: `/donasi/getData/`,
        success: function(res){
            $("#sp_result").empty();
            formatSemuaPermohonan(res)
        }
    })

    $('#search_submit').on('click', function(){
        let key = $("#search_input").val();
        if(key != ""){
            $.ajax({
                method: 'GET',
                url: `/donasi/getData/`,
                success: function(res){
                    $("#sp_result").empty();
                    formatSearchRecommendation(res, key)
    
                }
            })
        }
    })

})