from django.db import models

# Create your models here.
class Kategori(models.Model):
    nama_kategori = models.CharField(max_length=100)
    key_kategori = models.CharField(max_length=100)
    urgency = models.PositiveIntegerField()
    def __str__(self):
        return self.nama_kategori