from django.test import TestCase
from django.test import Client
from django.urls import resolve
from PermohonanDonasi.models import Permohonan
from .models import Kategori
from django.http import HttpRequest
from datetime import date
import unittest


# Create your tests here

class TestKategori(TestCase):

    def test_model(self):
        kategori = Kategori.objects.create(nama_kategori="nama", key_kategori="key", urgency=1)
        self.assertEqual('nama', str(kategori))
