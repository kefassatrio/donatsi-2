from django.contrib import admin
from django.urls import path, include

from django.conf.urls.static import  static
from . import settings
from PermohonanDonasi.views import homepage

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', homepage, name="homepage"), #homepage ntar
    path('tentang-kami/', include("TentangKami.urls", namespace="tentang-kami")),
    path('donasi/', include("PermohonanDonasi.urls", namespace="permohonan")),
    path('donasi/', include("PemberianDonasi.urls", namespace="pemberian")),
    path('user/', include("User.urls", namespace="user")),
    path('login/', include(('Login.urls', 'login'), namespace='login'))
]


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)