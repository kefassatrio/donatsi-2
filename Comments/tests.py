from django.test import TestCase
from django.test import Client
from django.urls import resolve
from PermohonanDonasi.views import detail_permohonan
from PermohonanDonasi.models import Permohonan
from .forms import add_comment
from .models import data_comments
from django.http import HttpRequest
from datetime import date
import unittest


# Create your tests here

class Comments(TestCase):
    def test_url_slash_donasi_is_exist(self):
        c = Client()
        response = c.get('/donasi/')
        self.assertEqual(response.status_code, 200)

    def test_url_slash_donasi_slash_id_is_exist(self):
        c = Client()
        permohonan = Permohonan.objects.create(nama_pemohon="Kefas Satrio", email="kefassatrio@gmail.com", bank="BCA", rekening=12345678910, nama_donasi="beliin telur dong mau bikin donat",target_uang=2500000, uang_terpenuhi=0, deadline="2020-12-12", deskripsi_alasan="mau makan")
        response = c.get('/donasi/' + str(permohonan.pk))
        self.assertEqual(response.status_code, 200)

    def test_detail_permohonan_has_form_comments(self):
        c = Client()
        permohonan = Permohonan.objects.create(nama_pemohon="Kefas Satrio", email="kefassatrio@gmail.com", bank="BCA", rekening=12345678910, nama_donasi="beliin telur dong mau bikin donat",target_uang=2500000, uang_terpenuhi=0, deadline="2020-12-12", deskripsi_alasan="mau makan")
        response = c.get('/donasi/' + str(permohonan.pk))
        content = response.content.decode('utf8')
        self.assertIn('<form', content)

    def test_forms_comments(self):
        data_form = {'comment':'comment', 'nama_user':'pemilik'}
        form = add_comment(data = data_form)
        self.assertTrue(form.is_valid())

    def test_button_submit_is_exist(self):
        c = Client()
        permohonan = Permohonan.objects.create(nama_pemohon="Kefas Satrio", email="kefassatrio@gmail.com", bank="BCA", rekening=12345678910, nama_donasi="beliin telur dong mau bikin donat",target_uang=2500000, uang_terpenuhi=0, deadline="2020-12-12", deskripsi_alasan="mau makan")
        response = c.get('/donasi/' + str(permohonan.pk))
        content = response.content.decode('utf8')
        self.assertIn('<button', content)

    def test_models_comments(self):
        permohonan = Permohonan.objects.create(nama_pemohon="Kefas Satrio", email="kefassatrio@gmail.com",bank="BCA", rekening=12345678910, nama_donasi="beliin telur dong mau bikin donat", target_uang=2500000, uang_terpenuhi=0, deadline="2020-12-12", deskripsi_alasan="mau makan")
        new_comment = data_comments.objects.create(permohonan_id=permohonan.pk, comment="test", pemilik="hanna")
        count_all_available_comment = data_comments.objects.all().count()
        self.assertEqual(count_all_available_comment, 1)
        self.assertEqual('test', str(new_comment))