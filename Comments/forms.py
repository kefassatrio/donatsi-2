from django import forms
from .models import data_comments

class add_comment(forms.ModelForm):
    class Meta:
        model = data_comments
        fields = ["comment"]
        exclude = ["pemilik"]