from django.db import models

# # Create your models here.
class data_comments(models.Model):
    permohonan = models.ForeignKey('PermohonanDonasi.Permohonan', on_delete=models.CASCADE, default=None, blank=True, null=True, related_name="pemohon")
    comment = models.TextField(blank=True, null=True, default=None)
    pemilik = models.TextField(blank=True, null=True, default=None)
    dateTime = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.comment