$(document).ready(()=>{
    const csrf = $('input[name="csrfmiddlewaretoken"]');

    $('#submit_saran').on('click',function(){
        let saran = $("#id_saran").val();
        if(saran != ""){
            $.ajax({
                method: 'POST',
                url: `/tentang-kami/`,
                data: {
                    csrfmiddlewaretoken : csrf[0].value,
                    saran: saran
                },
                success: function(res){
                    $("#id_saran").val('');
                    alert("Terima kasih atas saran anda :*")    
                }
            })
        }
    })
})