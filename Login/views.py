from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.views import auth_login
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.
response = {
    'author' : 'name'
}

def index(request):
    response['login'] = request.user.is_authenticated
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('homepage'))
    else :
        html = 'login.html'
    return render(request, html, response)

def homepage(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse('Login:login'))
    else:
        response['login'] = request.user.is_authenticated
        response['username'] = request.user.get_username
        return render(request, 'homepage.html')

def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        validate = authenticate(username=username, password=password)
        print(validate)
        if validate is not None:
            print("success")
            login(request, validate)
            print(request.user)
            return HttpResponseRedirect(reverse('homepage'))
        else:
            print("username/password wrong or does not exist")
            return HttpResponseRedirect(reverse('Login:login')) 

def logout_view(request):
	logout(request)
	return HttpResponseRedirect(reverse('homepage'))