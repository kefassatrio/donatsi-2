from django.conf.urls import url
from .views import index, homepage, login_view, logout_view

app_name = 'Login'

urlpatterns = [
    url(r'^$', index, name='login'),
    url(r'^validate/$', login_view, name='validate'),
    url(r'^logout/$', logout_view, name='logout')
]
